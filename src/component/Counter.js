import { useState } from "react";

const Counter = (props) => {
    const {sum, setSum} = props;
    const [number, setNumber] = useState(0);

    const handleIncrement = () => {
        setNumber(number + 1);
        setSum(sum + 1);
    }

    const handleDecrement = () => {
        setNumber(number - 1);
        setSum(sum - 1);
    }

    return (
        <div>
            <input type="button" onClick={handleIncrement}></input>
            {number}
            <input type="button" onClick={handleDecrement}></input>
        </div>  
    )
}

export default Counter;