import Counter from "./Counter";

const CounterGroup = (props) => {
    const {counterSize, sum, setSum} = props;
    
    const generateCounters = (counterSize) => {
        const newSize = parseInt(counterSize, 10)
        if(isNaN(newSize)) {
            return;
        }
        return new Array(newSize).fill('').map(
            (_, index) => {
                return <Counter key={index + newSize} sum={sum} setSum={setSum}></Counter>
            }
        )
    }

    return (
        <div>
            {generateCounters(counterSize)}
        </div>
    );
}

export default CounterGroup;