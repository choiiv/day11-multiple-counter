const CounterSizeGenerator = (props) => {
    const {counterSize, setCounterSize, setSum} = props;

    const handleSizeChange = (event) => {
        setCounterSize(event.target.value);
        setSum(0);
    }

    return (
        <div>
            <label>size:</label>
            <input name="size" value={counterSize} onChange={handleSizeChange}></input>
        </div>
    );
}

export default CounterSizeGenerator;