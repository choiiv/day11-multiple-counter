const CounterGroupSum = (props) => {
    const {sum} = props;
    return (
        <div>Sum: {sum}</div>
    );
}

export default CounterGroupSum;