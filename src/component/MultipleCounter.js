import { useState } from "react";
import CounterGroup from "./CounterGroup";
import CounterSizeGenerator from "./CounterSizeGenerator";
import CounterGroupSum from "./CounterGroupSum";

const MultipleCounter = () => {
    const [counterSize, setCounterSize] = useState("0");
    const [sum, setSum] = useState(0);

    return (
        <div>
            <CounterSizeGenerator counterSize={counterSize} setCounterSize={setCounterSize} setSum={setSum}></CounterSizeGenerator>
            <CounterGroupSum sum={sum}></CounterGroupSum>
            <CounterGroup counterSize={counterSize} sum={sum} setSum={setSum}></CounterGroup>
        </div>
    );
}

export default MultipleCounter;